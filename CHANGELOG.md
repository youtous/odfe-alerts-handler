# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v-1.1.2](https://gitlab.com/youtous/odfe-alerts-handler/-/tree/v-1.1.2) - 2022-07-24


### Added

- CHANGELOG

### Fixed

### Changed

- Update to go 1.18 and relative deps

### Removed